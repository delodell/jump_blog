<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'debsjump_wp3');

/** MySQL database username */
define('DB_USER', 'debsjump_wp3');

/** MySQL database password */
define('DB_PASSWORD', 'bl1zzard');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sYgnqoDEDe6QaSQ8DNUvGnH760nObQ2RYRZwh0gdoMwthn7dggQs7lwHS7Gb8ASQ');
define('SECURE_AUTH_KEY',  'Q3aoXAcjWmZtRaKtGKL5G4kbLWByS2tfdw1KGNUPUUVFWgio8lrZg46Sz4kZrA1t');
define('LOGGED_IN_KEY',    'Miatrgs4cnb9k8i8w24y5tRLVYj6AyIHUbI4Uz3jlVOlO6tQp34jVmlZPOn9AHp0');
define('NONCE_KEY',        'llRDYPnVerdECC4bjEgVAVnMxTpweV4WasOTRBlQhFwaokPxAtUXcpueCz6srLFa');
define('AUTH_SALT',        '1C2E4EpElwkwEO7dDrH23yFGv3R7O6hsS3jjipRbe9oz1xMtn3xM3uTjV0wJQiWE');
define('SECURE_AUTH_SALT', 'WDAfx0LeHGvJwg95PopOlQ2BOl9cXYETFw1eNmLO6Xe1ZqPNMJBtvIWSVvqIHtqD');
define('LOGGED_IN_SALT',   'ySjcV2NkXdWoZDFlcAO7xG7Ax3vZHqEJ8wpFFpujOZ7KBsAnhqZzVdI4ADwHstiB');
define('NONCE_SALT',       'cTHAsb9BoAaFo5Ayvm9GE93gCrHZehITiTWU1ddcW0vXZQ2KIzENv0LcZRxw3Lg8');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('WP_HOME','http://dev.jollyjumpsandbox.org/blog');
define('WP_SITEURL','http://dev.jollyjumpsandbox.org/blog');
